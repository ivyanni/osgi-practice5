package ru.tersoft.news.aif;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.tersoft.news.NewsService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of NewsService.
 * Returns news titles from Aif.ru.
 */
public class AifCounter implements NewsService {
    private final static String NEWS_URL = "http://www.aif.ru/rss/news.php";

    private String connect() throws IOException {
        URL url = new URL(NEWS_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        if (connection.getResponseCode() == 200) {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();
            return response.toString();
        } else {
            System.out.println(connection.getResponseMessage());
            return null;
        }
    }

    public List<String> getTitles() {
        try {
            String response = connect();
            if (response != null) {
                List<String> titles = new ArrayList<>();
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource source = new InputSource(new StringReader(response));
                NodeList items = builder.parse(source).getDocumentElement().getElementsByTagName("item");
                for (int i = 0; i < items.getLength(); i++) {
                    if (items.item(i).getNodeType() == Node.ELEMENT_NODE) {
                        Element item = (Element) items.item(i);
                        Element titleElem = (Element) item.getElementsByTagName("title").item(0);
                        titles.add(titleElem.getTextContent());
                    }
                }
                return titles;
            }
        } catch (IOException e) {
            System.out.println("Error occurred while fetching data from server: " + NEWS_URL);
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }
}
