package ru.tersoft.news.lenta;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.tersoft.news.NewsService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of NewsService.
 * Returns news titles from Lenta.ru.
 */
public class LentaCounter implements NewsService {
    private final static String NEWS_URL = "https://api.lenta.ru/lists/latest";

    private String connect() throws IOException {
        URL url = new URL(NEWS_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        if (connection.getResponseCode() == 200) {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();
            return response.toString();
        } else {
            System.out.println(connection.getResponseMessage());
            return null;
        }
    }

    public List<String> getTitles() {
        try {
            String response = connect();
            if (response != null) {
                List<String> titles = new ArrayList<>();
                JSONArray headlines = new JSONObject(response).getJSONArray("headlines");
                headlines.forEach((headline) -> {
                    JSONObject info = ((JSONObject) headline).getJSONObject("info");
                    String title = info.getString("title");
                    titles.add(title);
                });
                return titles;
            }
        } catch (IOException e) {
            System.out.println("Error occurred while fetching data from server: " + NEWS_URL);
        }
        return null;
    }
}
