package ru.tersoft.news.lenta;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import ru.tersoft.news.NewsService;

/**
 * @author ivyanni
 * @version 1.0
 * Bundle activator. Registers service implementation.
 */
public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(NewsService.class.getName(), new LentaCounter(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }
}
