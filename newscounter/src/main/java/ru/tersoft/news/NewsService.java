package ru.tersoft.news;

import java.util.List;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service which gets news titles from remote resource.
 */
public interface NewsService {
    List<String> getTitles();
}
