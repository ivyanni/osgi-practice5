package ru.tersoft.news;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service which counts words.
 */
public interface StatsService {
    void stats(String source);
    void stats();
}
