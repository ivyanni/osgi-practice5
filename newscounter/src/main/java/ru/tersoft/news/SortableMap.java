package ru.tersoft.news;

import java.util.*;

/**
 * @author ivyanni
 * @version 1.0
 * LinkedHashMap with ability to sort by value.
 */
public class SortableMap<K, V extends Comparable<V>> extends LinkedHashMap<K, V> {
    public SortableMap() {
    }

    public void sortByValue() {
        List<Map.Entry<K, V>> list = new LinkedList<>(entrySet());
        list.sort((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()));
        clear();
        for (Map.Entry<K, V> entry : list) {
            put(entry.getKey(), entry.getValue());
        }
    }
}
