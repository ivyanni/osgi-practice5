package ru.tersoft.news.impl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import ru.tersoft.news.NewsService;
import ru.tersoft.news.StatsService;

import java.util.Hashtable;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi bundle activator.
 * Opens Service tracker for NewsService implementations search.
 * Registers service and binds it to Gogo command on bundle start.
 */
public class Activator implements BundleActivator {
    private ServiceTracker tracker;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        Hashtable<String, String> props = new Hashtable<>();
        props.put("osgi.command.scope", "news");
        props.put("osgi.command.function", "stats");
        tracker = new ServiceTracker(bundleContext, NewsService.class.getName(), null);
        tracker.open();
        bundleContext.registerService(StatsService.class.getName(), new StatsServiceImpl(tracker), props);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        tracker.close();
    }
}
