package ru.tersoft.news.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import ru.tersoft.news.NewsService;
import ru.tersoft.news.SortableMap;
import ru.tersoft.news.StatsService;

import java.util.*;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of service which counts words in news titles
 * from selected sources and prints the most frequent to console.
 */
public class StatsServiceImpl implements StatsService {
    private final static int OUTPUT_AMOUNT = 10;
    private ServiceTracker tracker;

    StatsServiceImpl(ServiceTracker tracker) {
        this.tracker = tracker;
    }

    private String getReferenceShortName(ServiceReference reference) {
        String fullName = reference.getBundle().getSymbolicName();
        return fullName.substring(fullName.lastIndexOf(".") + 1);
    }

    public void stats(String source) {
        ServiceReference[] references = tracker.getServiceReferences();
        List<String> titles = new ArrayList<>();
        for (ServiceReference reference : references) {
            if (getReferenceShortName(reference).equals(source)) {
                System.out.print("Wait...");
                NewsService service = (NewsService) tracker.getService(reference);
                List<String> serviceTitles = service.getTitles();
                if(serviceTitles != null) {
                    titles.addAll(serviceTitles);
                }
                SortableMap<String, Integer> map = new SortableMap<>();
                map = countWords(map, titles);
                map.sortByValue();
                printResult(map);
                return;
            }
        }
        System.out.println("Service with entered name is unavailable");
    }

    public void stats() {
        ServiceReference[] references = tracker.getServiceReferences();
        if (references != null) {
            List<String> titles = new ArrayList<>();
            Integer enteredIndex = getSourceIndex(references);
            if (enteredIndex == null) {
                return;
            }
            System.out.print("Wait...");
            if (enteredIndex == 0) {
                for (ServiceReference reference : references) {
                    NewsService service = (NewsService) tracker.getService(reference);
                    List<String> serviceTitles = service.getTitles();
                    if(serviceTitles != null) {
                        titles.addAll(serviceTitles);
                    }
                }
            } else {
                NewsService service = (NewsService) tracker.getService(references[enteredIndex-1]);
                List<String> serviceTitles = service.getTitles();
                if(serviceTitles != null) {
                    titles.addAll(serviceTitles);
                }
            }
            SortableMap<String, Integer> map = new SortableMap<>();
            map = countWords(map, titles);
            map.sortByValue();
            printResult(map);
        } else {
            System.out.println("Available sources were not found");
        }
    }

    private Integer getSourceIndex(ServiceReference[] references) {
        System.out.println("Available sources:");
        int sourceCounter = 0;
        if (references.length > 1) {
            sourceCounter++;
            System.out.println("0. all");
        }
        for (ServiceReference reference : references) {
            System.out.println(sourceCounter + ". " + getReferenceShortName(reference));
            sourceCounter++;
        }
        System.out.print("Enter appropriate number: ");
        String answer = System.console().readLine();
        int enteredIndex;
        try {
            enteredIndex = Integer.parseInt(answer);
            if (enteredIndex < 0 || enteredIndex >= sourceCounter) {
                System.out.println("Wrong number");
                return null;
            }
        } catch (NumberFormatException e) {
            System.out.println("You must enter a number");
            return null;
        }
        return enteredIndex;
    }

    private SortableMap<String, Integer> countWords(SortableMap<String, Integer> map, List<String> titles) {
        for (String title : titles) {
            title = title.toLowerCase().replaceAll("[!?,\"\\u00ab\\u00bb/:#.]", "");
            String[] words = title.split("\\s+");
            for (String word : words) {
                if (word.length() == 1) {
                    char c = word.toCharArray()[0];
                    if (!Character.isLetter(c)) {
                        continue;
                    }
                }
                if (map.containsKey(word)) {
                    int value = map.get(word);
                    map.replace(word, value + 1);
                } else {
                    map.put(word, 1);
                }
            }
        }
        return map;
    }

    private void printResult(Map<String, Integer> map) {
        Iterator it = map.keySet().iterator();
        int i = 0;
        System.out.print("\b\b\b\b\b\b\b");
        while (it.hasNext() && i < OUTPUT_AMOUNT) {
            String key = (String) it.next();
            System.out.print(i + 1 + ". " + key + ": " + map.get(key) + "\n");
            i++;
        }
    }
}
